﻿namespace GeographicLib

[<Measure>] type rad
[<Measure>] type deg
[<Measure>] type km
[<Measure>] type m
